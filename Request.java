
public class Request
{
	private int LBA;
	private double age;
	public int getLBA() {
		return LBA;
	}
	public void setLBA(int lBA) {
		LBA = lBA;
	}
	public double getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Request(int lBA, double age) {
		super();
		LBA = lBA;
		this.age = age;
	}
	
	public boolean equals(Object r)
	{
		if(! (r instanceof Request))
			return false;
		
		if(r == this)
			return true;
		
		Request req = (Request) r;
		
		return (req.getLBA() == LBA && Math.abs(req.getAge() - age) < 0.001);
	}
	
	public int hashCode()
	{
		return LBA + (int) age;
	}
	
	public void age_up(double up)
	{
		age += up;
		
	}
	
}
