all: *.java
	javac Request.java
	javac PhysicalAddress.java
	javac Disk.java
	javac Simulator.java
	javac Tester.java

clean:
	rm *.class
