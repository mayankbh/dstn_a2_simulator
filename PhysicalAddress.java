
public class PhysicalAddress 
{
	private int cylinder;
	private int head;
	private int sector;
	public int getCylinder() {
		return cylinder;
	}
	public void setCylinder(int cylinder) {
		this.cylinder = cylinder;
	}
	public PhysicalAddress(int cylinder, int head, int sector) {
		this.cylinder = cylinder;
		this.head = head;
		this.sector = sector;
	}
	public int getHead() {
		return head;
	}
	public void setHead(int head) {
		this.head = head;
	}
	public int getSector() {
		return sector;
	}
	public void setSector(int sector) {
		this.sector = sector;
	}
}
