import java.util.ArrayList;


public class Disk 
{
	private int cylinders;
	private int heads_per_cylinder;
	private int sectors_per_track;
	
	private double head_switch_time;		
	private double seek_time;			//Time from outermost to innermost
	private double rotation_time;		//Time for one full rotation
	
	private int current_cylinder;
	private int current_head;
	private int current_sector;
	private ArrayList<Integer> cache;
	private int cache_size;
	
	//TODO:Constructor
	
	public PhysicalAddress LBA_to_PA(int LBA)
	{
		int C, H, S;
		C = LBA / (heads_per_cylinder * sectors_per_track);
		H = (LBA / sectors_per_track) % heads_per_cylinder;
		S = (LBA % sectors_per_track) + 1;
		
		return new PhysicalAddress(C, H , S);
	}

	public Disk(int cylinders, int heads_per_cylinder, int sectors_per_track,
			double head_switch_time, double seek_time, double rotation_time,
			int current_cylinder, int current_head, int current_sector,
			int cache_size) {
		super();
		this.cylinders = cylinders;
		this.heads_per_cylinder = heads_per_cylinder;
		this.sectors_per_track = sectors_per_track;
		this.head_switch_time = head_switch_time;
		this.seek_time = seek_time;
		this.rotation_time = rotation_time;
		this.current_cylinder = current_cylinder;
		this.current_head = current_head;
		this.current_sector = current_sector;
		this.cache_size = cache_size;
		this.cache = new ArrayList<Integer>();
	}

	public double getSeekTime(int LBA)		//Account for only seek
	{
		double temp_time = 0;
		PhysicalAddress PA = LBA_to_PA(LBA);
		
		int cylinder_diff = current_cylinder - PA.getCylinder();
		if(cylinder_diff < 0)
			cylinder_diff = -cylinder_diff;
		
		//Time between 0 to innermost cylinder = seek_time
		//Hence time to cover cylinder_diff = cylinder_diff/innermost cylinder number * seek_time
		//Innermost cylinder numbered by (total number of cylinders - 1) index
		
		double new_seek_time = ((double)cylinder_diff/(double)(cylinders - 1) ) * seek_time;
		
		temp_time += new_seek_time;
		
		if(PA.getHead() != current_head)	//Need to add head switch time
			temp_time += head_switch_time;
		
		return temp_time;
	}
	
	public double getPositionTime(int LBA)	//Account for both seek and rotation
	{
		double temp_time = 0;
		PhysicalAddress PA = LBA_to_PA(LBA);
		
		int cylinder_diff = current_cylinder - PA.getCylinder();
		if(cylinder_diff < 0)
			cylinder_diff = -cylinder_diff;
		
		//Time between 0 to innermost cylinder = seek_time
		//Hence time to cover cylinder_diff = cylinder_diff/innermost cylinder number * seek_time
		//Innermost cylinder numbered by (total number of cylinders - 1) index
		
		double new_seek_time = ((double)cylinder_diff/(double)(cylinders - 1) ) * seek_time;
		
		temp_time += new_seek_time;
		
		if(PA.getHead() != current_head)	//Need to add head switch time
			temp_time += head_switch_time;
		
		int sector_diff = current_sector - PA.getSector();
		if(sector_diff < 0)	//Wrapped around
			sector_diff += sectors_per_track;
		
		//rotation_time needed to cover sectors_per_track sectors
		//Hence (sector_diff/sectors_per_track)*rotation_time for sector_diff sectors
		
		double new_rotate_time = ((double) sector_diff/(double)sectors_per_track) * rotation_time;
		
		temp_time += new_rotate_time;	//Add this rotation time
		
		return temp_time;
	}
	
	public boolean cache_lookup(int LBA)
	{
		if(cache.contains(LBA))
			return true;
		else
			return false;
	}
	
	public void cache_add(int LBA)
	{
		if(cache.size() == cache_size)	//Cache is full, replace an entry
		{
			cache.remove(0);
			cache.add(LBA);
			return;
		}
		
		//Cache not full
		cache.add(LBA);
	}
	
	public double disk_access(int LBA)
	{
		PhysicalAddress PA = LBA_to_PA(LBA);
		double time = getPositionTime(LBA);
		
		this.current_cylinder = PA.getCylinder();
		this.current_head = PA.getHead();			//Update hardware positions
		this.current_sector = PA.getSector();
		
		cache_add(LBA);							//Cache request
		
		return time;
	}
	
	public void print_details()
	{
		System.out.println("Cylinders: " + cylinders);
		System.out.println("HPC: " + heads_per_cylinder);
		System.out.println("SPT: " + sectors_per_track);
		System.out.println("Head switch time: " + head_switch_time);
		System.out.println("Seek time: " + seek_time);
		System.out.println("Rotation time: " + rotation_time);
		System.out.println("Cache Size: " + cache_size);
	}
}
