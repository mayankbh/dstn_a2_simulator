import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Queue;
import java.util.Scanner;


public class Simulator 
{
	private Disk disk;
	int scheduling_policy;
	ArrayList<Request> request_queue;
	double simulation_time;
	
	String param_file = "disk.par";
	String request_file = "request.txt";
	
	public static final int ASPCTF_OPT = 1;
	public static final int SPTF_ROT_OPT = 2;
	
	public static final double AGE_WEIGHT = 0.001;
	public static final double LATENCY_WEIGHT = 0.001;
	
	//TODO:Constructor should read the parameter file and create the disk with required specifications
	
	public Simulator(String param, String request)
	{
		this.param_file = param;
		this.request_file = request;
		
		File input = new File(param_file);
		Scanner in = null;
		
		try
		{
			in = new Scanner(input);
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Error opening parameter file " + param_file);
		}
		
		int cylinders = in.nextInt();
		int heads_per_cylinder = in.nextInt();
		int sectors_per_track = in.nextInt();
		
		double head_switch_time = in.nextDouble();		
		double seek_time = in.nextDouble();			//Time from outermost to innermost
		double rotation_time = in.nextDouble();		//Time for one full rotation
		
		int cache_size = in.nextInt();
		this.scheduling_policy = in.nextInt();

		this.disk = new Disk(cylinders, heads_per_cylinder, sectors_per_track,
						head_switch_time, seek_time, rotation_time, 0, 0, 1, cache_size);
		
		this.request_queue = new ArrayList<Request>();
		
		if(in != null)
			in.close();
	}
	
	public Request schedule_ASPCTF_OPT()	//Return next request to be scheduled according to ASPCTF_OPT
	{
		if(request_queue.isEmpty())
		{
			System.out.println("Sched:Request queue is empty");
			return null;
		}
		Request best_request = null;
		double best_time = Double.POSITIVE_INFINITY;
		double temp_time;
		
		for(Request request : request_queue)	//Iterate over requests
		{
			if(disk.cache_lookup(request.getLBA()))
			{
				return request;			//Caching
			}
			
			temp_time = disk.getPositionTime(request.getLBA());		//Positioning			
			
			temp_time -= request.getAge() * AGE_WEIGHT;		//Aging
			
			if(temp_time < best_time)
			{
				best_request = request;
				best_time = temp_time;		//Update best candidate
			}
			
		}
		
		return best_request;
	}
	
	public Request schedule_SPTF_ROT_OPT()
	{
		if(request_queue.isEmpty())
		{
			System.out.println("Sched:Request queue is empty");
			return null;
		}
		Request best_request = request_queue.get(0);
		double best_time = Float.POSITIVE_INFINITY;
		double temp_delay, temp_seek;
		double best_subtract = 0;
		
		for(Request request : request_queue)	//Iterate over requests
		{
			
			temp_delay = disk.getPositionTime(request.getLBA());		//Positioning
			temp_seek = disk.getSeekTime(request.getLBA());			
			
			if(temp_delay - ((temp_delay - temp_seek) * LATENCY_WEIGHT) < best_time - best_subtract )
			{
				best_request = request;
				best_time = temp_delay;		//Update best candidate
				best_subtract = (temp_delay - temp_seek) * LATENCY_WEIGHT;
			}
			
			
		}
		
		return best_request;
	}
	
	
	public void run_simulation()
	{
		//Keep running until the request queue is empty and there is nothing left in the request file
		int next_LBA = 0;
		double next_arrival = 0;
		simulation_time = 0;
		Request next_request = null;
		double latest_request_time;
		ArrayList<Request> pending_requests = new ArrayList<Request>();
		
		File input_file = new File(request_file);
		Scanner in = null;
		
		print_details();
		
		try
		{
			in = new Scanner(input_file);
		}
		catch(FileNotFoundException e)
		{
			System.out.println("Error opening input file " + request_file);
		}
		
		while(in.hasNext() || !request_queue.isEmpty() || !pending_requests.isEmpty())
		{
			if(in.hasNextInt())
			{
				next_LBA = in.nextInt();
				next_arrival = in.nextDouble();
				System.out.println("Adding request for " + next_LBA + " to pending queue");
				pending_requests.add(new Request(next_LBA, next_arrival));
			}
			else
			{
				next_arrival = Double.POSITIVE_INFINITY;
			}
		//	System.out.println(next_arrival + " < " + simulation_time);
			
			while(!pending_requests.isEmpty() && pending_requests.get(0).getAge() < simulation_time)
			{
				//Add it to request queue
				System.out.println(simulation_time + ": Adding request for " + next_LBA + " to queue.");
				pending_requests.get(0).setAge(0);
				request_queue.add(pending_requests.get(0));
				pending_requests.remove(0);
			}
			//No more pending requests to add, now get the next request
			System.out.println(simulation_time + ": Entering scheduler, queue has " + request_queue.size() + " elements.");
			if(scheduling_policy == ASPCTF_OPT)
				next_request = schedule_ASPCTF_OPT();
			else if(scheduling_policy == SPTF_ROT_OPT)
				next_request = schedule_SPTF_ROT_OPT();
			
			if(next_request == null)
			{
				System.out.println(simulation_time + ": Received null request to schedule, idling");
				simulation_time += 0.1;
				continue;
			}
			//Now dispatch the request
			latest_request_time = disk.disk_access(next_request.getLBA());
			
			//Remove it from the queue
			request_queue.remove(next_request);
			
			//Age the remaining requests
			age_all_requests(latest_request_time);
			
			System.out.println(simulation_time + ": Scheduling request for LBA " + next_request.getLBA());
			
			//Update simulation time
			simulation_time += latest_request_time;
		}
		
		System.out.println("Finished running simulation");
		if(in != null)
			in.close();
	}
	
	public void age_all_requests(double delta)
	{
		for(Request r: request_queue)
		{
			r.age_up(delta);
		}
	}
	
	public void print_details()
	{
		disk.print_details();
		System.out.println("Scheduling Policy: " + scheduling_policy);
	}
}
