
public class Tester 
{
	public static void main(String[] args)
	{
		//args[0] - param
		//args[1] - request
		
		if(args.length != 2)
		{
			System.out.println("Usage : java Tester.class <Param file> <Request file>");
			return;
		}
		
		Simulator sim = new Simulator(args[0], args[1]);
		sim.run_simulation();
		
		System.out.println("Done simulating");
		
	}
}
